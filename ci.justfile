deploy:
    #!/usr/bin/bash
    set -o errexit
    set -o nounset
    set -o pipefail

    declare -r ref="$(basename -- "${GIT_REF}")"
    python3 -m venv buildvenv
    buildvenv/bin/pip3 install --no-cache-dir . beautifulsoup4 pygments
    buildvenv/bin/stag --version
    buildvenv/bin/stag -C doc build --url="https://pages.goral.net.pl/stag/${ref}"
    install-artifacts doc/build "${ref}"
