# SPDX-License-Identifier: GPL-3.0-only
# Copyright (C) 2021 Michał Góral.

try:
    import importlib.metadata as importlib_metadata
except ModuleNotFoundError:
    import importlib_metadata

__version__ = importlib_metadata.version("stag-ssg")
