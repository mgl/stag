# SPDX-License-Identifier: GPL-3.0-only
# Copyright (C) 2021 Michał Góral.


class StagError(Exception):
    pass


class StagPluginError(Exception):
    pass
