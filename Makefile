all: test black build

test:
	@poetry run pytest -n auto

coverage:
	@poetry run pytest -n auto --cov=stag --cov-branch --cov-report=html

black:
	@poetry run black src tests

build:
	poetry build

clean:
	rm -rf build dist .eggs poetry.lock

publish: clean
	poetry build
	poetry publish

docs:
	poetry run stag -C doc build

docs-serve:
	poetry run stag -C doc serve

.PHONY: all test black build clean publish docs
